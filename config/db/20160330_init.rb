Sequel.migration do
  change do
    create_table(:provider) do
      primary_key :provider_id, :type=>"mediumint(8) unsigned"
      column :provider_url, "varchar(128)", :null=>false
      column :provider_version, "timestamp", :default=>Sequel::CURRENT_TIMESTAMP, :null=>false
    end
    
    create_table(:stop_times) do
      column :provider_id, "mediumint(8) unsigned", :null=>false
      column :trip_id, "int(10) unsigned", :null=>false
      column :arrival_time, "time", :null=>false
      column :departure_time, "time", :null=>false
      column :stop_id, "int(10) unsigned", :null=>false
      column :stop_sequence, "smallint(11) unsigned", :null=>false
      column :stop_headsign, "varchar(64)"
      column :pickup_type, "tinyint(3) unsigned", :default=>0, :null=>false
      column :drop_off_type, "tinyint(3) unsigned", :default=>0, :null=>false
      column :shape_dist_traveled, "float unsigned"
      column :timepoint, "tinyint(3) unsigned", :default=>1, :null=>false
      
      primary_key [:provider_id, :trip_id, :stop_id, :stop_sequence]
      
      index [:arrival_time], :name=>:arrival_time
      index [:departure_time], :name=>:departure_time
    end
    
    create_table(:stops) do
      column :provider_id, "mediumint(8) unsigned", :null=>false
      column :stop_id, "int(10) unsigned", :null=>false
      column :stop_code, "varchar(32)"
      column :stop_name, "varchar(96)", :null=>false
      column :stop_desc, "varchar(128)"
      column :stop_pt, "point", :null=>false
      column :zone_id, "int(10) unsigned"
      column :stop_url, "varchar(128)"
      column :location_type, "tinyint(3) unsigned", :default=>0, :null=>false
      column :parent_station, "tinyint(3) unsigned", :default=>0, :null=>false
      column :stop_timezone, "varchar(29)"
      column :wheelchair_boarding, "tinyint(3) unsigned", :default=>0, :null=>false
      
      primary_key [:provider_id, :stop_id]
    end
    
    create_table(:agency) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :agency_id, "int(10) unsigned", :null=>false
      column :agency_name, "varchar(64)", :null=>false
      column :agency_url, "varchar(128)", :null=>false
      column :agency_timezone, "varchar(29)", :null=>false
      column :agency_lang, "char(2)"
      column :agency_phone, "varchar(16)"
      column :agency_fare_url, "varchar(128)"
      column :agency_email, "varchar(64)"
      
      primary_key [:provider_id, :agency_id]
    end
    
    create_table(:calendar) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :service_id, "int(10) unsigned", :null=>false
      column :weekdays, "set('monday','tuesday','wednesday','thursday','friday','saturday','sunday')", :null=>false
      column :monday, "tinyint(1)", :null=>false
      column :tuesday, "tinyint(1)", :null=>false
      column :wednesday, "tinyint(1)", :null=>false
      column :thursday, "tinyint(1)", :null=>false
      column :friday, "tinyint(1)", :null=>false
      column :saturday, "tinyint(1)", :null=>false
      column :sunday, "tinyint(1)", :null=>false
      column :start_date, "date", :null=>false
      column :end_date, "date", :null=>false
      
      primary_key [:provider_id, :service_id]
      
      index [:start_date, :end_date, :weekdays], :name=>:start_end_weekdays
    end
    
    create_table(:calendar_dates) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :service_id, "int(10) unsigned", :null=>false
      column :date, "date", :null=>false
      column :exception_type, "tinyint(3) unsigned", :null=>false
      
      primary_key [:provider_id, :service_id, :date]
      
      index [:date], :name=>:date
    end
    
    create_table(:fare_attributes) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :fare_id, "int(10) unsigned", :null=>false
      column :price, "decimal(10,2)", :null=>false
      column :currency_type, "char(3)", :null=>false
      column :payment_method, "tinyint(3) unsigned", :null=>false
      column :transfers, "tinyint(4)", :null=>false
      column :transfer_duration, "mediumint(8) unsigned"
      
      primary_key [:provider_id, :fare_id]
    end
    
    create_table(:fare_rules) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :fare_id, "int(10) unsigned", :null=>false
      column :route_id, "int(10) unsigned"
      column :origin_id, "int(10) unsigned"
      column :destination_id, "int(10) unsigned"
      column :contains_id, "int(10) unsigned"
      
      primary_key [:provider_id, :fare_id]
    end
    
    create_table(:feed_info) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :feed_publisher_name, "varchar(32)", :null=>false
      column :feed_publisher_url, "varchar(128)", :null=>false
      column :feed_lang, "varchar(24)", :null=>false
      column :feed_start_date, "date"
      column :feed_end_date, "date"
      column :feed_version, "varchar(16)"
      
      primary_key [:provider_id, :feed_publisher_name]
    end
    
    create_table(:frequencies) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :trip_id, "int(10) unsigned", :null=>false
      column :start_time, "time", :null=>false
      column :end_time, "time", :null=>false
      column :headway_secs, "mediumint(9)", :null=>false
      column :exact_times, "tinyint(4)", :default=>0, :null=>false
      
      primary_key [:provider_id, :trip_id]
    end
    
    create_table(:routes) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :route_id, "int(10) unsigned", :null=>false
      column :agency_id, "int(10) unsigned"
      column :route_short_name, "varchar(32)", :null=>false
      column :route_long_name, "varchar(96)", :null=>false
      column :route_desc, "varchar(128)"
      column :route_type, "tinyint(3) unsigned", :null=>false
      column :route_url, "varchar(128)"
      column :route_color, "char(6)"
      column :route_text_color, "char(6)"
      
      primary_key [:provider_id, :route_id]
    end
    
    create_table(:transfers) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :from_stop_id, "int(10) unsigned", :null=>false
      column :to_stop_id, "int(10) unsigned", :null=>false
      column :transfer_type, "tinyint(3) unsigned", :default=>0, :null=>false
      column :min_transfer_time, "smallint(5) unsigned"
      
      primary_key [:provider_id, :from_stop_id, :to_stop_id]
    end
    
    create_table(:trips) do
      foreign_key :provider_id, :provider, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :route_id, "int(10) unsigned", :null=>false
      column :service_id, "int(10) unsigned", :null=>false
      column :trip_id, "int(10) unsigned", :null=>false
      column :trip_headsign, "varchar(64)"
      column :trip_short_name, "varchar(32)"
      column :direction_id, "tinyint(3) unsigned"
      column :block_id, "int(10) unsigned"
      column :shape_id, "int(10) unsigned"
      column :wheelchair_accessible, "tinyint(3) unsigned", :default=>0
      column :bikes_allowed, "tinyint(3) unsigned", :default=>0, :null=>false
      
      primary_key [:provider_id, :trip_id]
      
      index [:service_id], :name=>:service_id
    end
    
    create_table(:shapes) do
      foreign_key :provider_id, :agency, :type=>"mediumint(8) unsigned", :null=>false, :key=>[:provider_id]
      column :shape_id, "int(10) unsigned", :null=>false
      column :shape_pt, "point", :null=>false
      column :shape_pt_sequence, "smallint(5) unsigned", :null=>false
      column :shape_dist_traveled, "float unsigned"
      
      primary_key [:provider_id, :shape_id]
    end
  end
end
