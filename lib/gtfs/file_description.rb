
class GTFS
class DBImport

FILE_DESCRIPTION = {
    'agency.txt'          => {
        required: true,
           index: { :agency_id             => :agency  },
           types: { :agency_name           => :string,
                    :agency_url            => :url,
                    :agency_timezone       => :ascii,
                    :agency_lang           => :ascii,
                    :agency_phone          => :string,
                    :agency_fare_url       => :url,
                    :agency_email          => :email   } },
    'stops.txt'           => {
        required: true,
           index: { :stop_id               => :stop,
                    :zone_id               => :zone    },
           types: { :stop_code             => :string,
                    :stop_name             => :string,
                    :stop_desc             => :string,
                    :stop_url              => :url,
                    :location_type         => :int,
                    :stop_timezone         => :ascii,
                 :wheelchair_boarding      => :int  },
         sqlconv: ->(x) {
            x[:parent_station] ||= 1
            lat = x.delete(:stop_lat)
            lon = x.delete(:stop_lon)
            pt  = Sequel.function(:GeomFromText,
                                  "POINT(#{lon} #{lat})", 4326)
            x[:stop_pt] = pt }
    },
    'routes.txt'          => {
        required: true,
           index: { :route_id              => :route   },
            refs: { :agency_id             => :agency  },
           types: { :route_short_name      => :string,
                    :route_long_name       => :string,
                    :route_desc            => :string,
                    :route_type            => :int,
                    :route_url             => :url,
                    :route_color           => :ascii,
                    :route_text_color      => :ascii   } },
    'trips.txt'           => {
        required: true,
           index: { :trip_id               => :trip,
                    :block_id              => :block   },
            refs: { :route_id              => :route,
                    :service_id            => :service,
                    :shape_id              => :shape   },
           types: { :trip_headsign         => :string,
                    :trip_short_name       => :string,
                    :wheelchair_accessible => :int,
                    :bikes_allowed         => :int     }
    },
    'stop_times.txt'      => {
        required: true,
            refs: { :trip_id              => :trip,
                    :stop_id              => :stop    },
           types: { :arrival_time         => :time,
                    :departure_time       => :time,
                    :stop_sequence        => :int,
                    :stop_headsigne       => :string,
                    :pickup_type          => :int,
                    :drop_off_type        => :int,
                    :shape_dist_traveled  => :float,
                    :timepoint            => :int     }
    },
    'calendar.txt'        => {
        required: true,
           index: { :service_id           => :service },
           types: { :monday               => :bool,
                    :tuesday              => :bool,
                    :wednesday            => :bool,
                    :thursday             => :bool,
                    :friday               => :bool,
                    :saturday             => :bool,
                    :sunday               => :bool,
                    :start_date           => :date,
                    :end_date             => :date    },
         sqlconv: ->(x) {
            days = [ :monday, :tuesday, :wednesday, :thursday, :friday,
                     :saturday, :sunday
                   ].reject{|day| !x[day] }.map{|k| k.to_s }.join(',')
            x[:weekdays] = days
        }

    },
    'calendar_dates.txt'  => {
        required: false,
            refs: { :service_id           => :service },
           types: { :date                 => :date,
                    :exception_type       => :int     }
    },
    'fare_attributes.txt' => {
        required: false,
           index: { :fare_id              => :fare    },
           types: { :price                => :decimal,
                    :currency_type        => :ascii,
                    :payment_method       => :int,
                    :transfers            => :int,
                    :transfer_duration    => :int }
    },
    'fare_rules.txt'      => {
        required: false,
            refs: { :fare_id              => :fare,
                    :route_id             => :route,
                    :origin_id            => :zone,
                    :destination_id       => :zone,
                    :contains_id          => :zone    }
    },
    'shapes.txt'          => {
        required: false,
           index: { :shape_id             => :shape   },
           types: { :shape_pt_sequence    => :int,
                    :shape_dist_traveled  => :float   }, 
         sqlconv: ->(x) {
            lat = x.delete(:shape_pt_lat)
            lon = x.delete(:shape_pt_lon)
            pt  = Sequel.function(:GeomFromText,
                                  "POINT(#{lon} #{lat})", 4326)
            x[:shape_pt] = pt }
    },
    'frequencies.txt'     => {
        required: false,
            refs: { :trip_id              => :trip    },
           types: { :start_time           => :time,
                    :end_time             => :time,
                    :headway_secs         => :int,
                    :exact_times          => :int     }
    },
    'transfers.txt'       => {
        required: false,
            refs: { :from_stop_id         => :stop,
                    :to_stop_id           => :stop    },
           types: { :transfer_type        => :int, 
                    :min_transfer_time    => :int     }
    } ,
    'feed_info.txt'       => {
        required: false,
           types: { :feed_publisher_name  => :string,
                    :feed_publisher_url   => :url,
                    :feed_lang            => :ascii,
                    :feed_start_date      => :date,
                    :feed_end_date        => :date,
                    :feed_version         => :string }
    },
}

end
end
