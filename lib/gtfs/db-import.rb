require 'date'
require 'csv'
require 'set'
require 'zip'

require_relative 'file_description'

class GTFS
class DBImport
    def initialize(db, logger: NULL_LOGGER)
        @db     = db
        @logger = logger
    end

    def remove(provider)
        FILE_DESCRIPTION.each {|filename, desc|
            table   = filename.sub(/\.txt$/, '').to_sym            
            @db[table].where(:provider_id => provider).delete
        }
    end

    def import(provider, gtfs_file, encoding: Encoding.default_external)
        zip = Zip::File.open(gtfs_file)

        # Get index mapping
        reidx = reindex(zip)

        # Ensure that previous data are removed
        remove(provider)
        
        # Create zip entrie hash indexed by filename
        zip_entries = Hash[zip.entries.map {|e| [e.name, e] }]

        # Process all GTFS files present in the zip
        FILE_DESCRIPTION.map{|name, desc|
            if entry = zip_entries[name]
                [ entry, desc ]
            end
        }.compact.each do |entry, desc|
            # Construct table name from filename
            table   = entry.name.sub(/\.txt$/, '').to_sym

            # Retrieve: field index -> index map
            idx_map = (desc[:index] || {}).merge(desc[:refs ] || {})
            
            # Create CSV reader
            csv     = CSV::new(entry.get_input_stream,
                               :headers=>true, :return_headers=>false,
                               :header_converters => [:downcase, :symbol])

            # Process CSV row
            csv.each {|row|
                # Reindex row index fields
                row = catch(:failed) do
                    Hash[row.map {|field,value|
                        # If it is an index field, perform index mapping
                        if name = idx_map[field]
                            # Got a remapping index?
                            if idx = reidx[name]
                                unless value = idx[v = value]
                                    # Got a reference to an index value
                                    #  which is not defined!
                                    throw :failed,
                                          "missing index for #{name}[#{v}]"
                                end

                            # No remapping index and no value?
                            elsif value.nil? || value.empty?
                                # That's ok, only ensure value is nil
                                value = nil

                            # Here comes troubles
                            else
                                raise "Index was not defined for #{name}"
                            end
                        end
                        # Returns value
                        [field, value]
                    }]
                end

                # Check that row reindexing was ok
                # If we got a string that's a recoverable error
                # row will just be discarded
                if String === row 
                    @logger.warn(row)
                    next
                end

                # Fixing value according to type
                (desc[:types] || {}).each {|field, type|
                    # Skip if field has no value
                    next if row[field].nil?
                    # Perform conversion
                    #  (only problematic types are handled)
                    case type
                    when :string
                        # Sometimes we don't get the right encoding
                        # Fix it to the supposed default file encoding
                        if row[field].encoding ==  Encoding::ASCII_8BIT
                            row[field].force_encoding(encoding)
                        end
                    when :bool
                        row[field] = Integer(row[field]) != 0
                    when :int
                        row[field] = Integer(row[field])
                    end
                }

                # Apply the special SQL convertion
                if desc[:sqlconv]
                    desc[:sqlconv].call(row)
                end

                # Add provider field
                row[:provider_id] = provider

                # Reject all nil value
                #  (so that database is able to apply default value)
                row.reject! {|k,v| v.nil? }
                
                # At least ... insert
                @db[table].insert(row)
            }
        end
    ensure
        zip.close
    end


    private

    def reindex(zip)        
        reidx = {}
        zip.each do |entry|
            # Only process file if it is part of GTFS
            # and it provides index definition
            next unless desc = FILE_DESCRIPTION[entry.name]
            next unless index = desc[:index]    

            # Create empty set for each index key defined in the file
            idx_list = Hash[index.keys.map {|k| [k, Set.new ]}]

            # Create CSV reader
            csv     = CSV::new(entry.get_input_stream,
                               :headers => true, :return_headers => false,
                               :header_converters => [:downcase, :symbol])

            # Build index list from each CSV row
            csv.each {|row| idx_list.each {|id, set| set.add(row[id]) } }

            # Convert index list to index mapping 
            index.each {|id, name|
                reidx[name] = Hash[idx_list[id].each.with_index.map {|*a| a}]
            }    
        end

        # Returns index remapping
        reidx
    end

end
end
