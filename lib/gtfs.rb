require 'logger'

class GTFS
    class NullLoger < Logger
        def initialize(*args)  ; end
        def add(*args, &block) ; end
    end
    NULL_LOGGER = NullLoger.new
end
