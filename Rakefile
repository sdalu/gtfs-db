# -*- ruby -*-

require 'yaml'
require 'pathname'
require 'uri'
require 'date'
require 'digest'

DOWNLOAD_DIR = 'downloads'

$cfg = YAML.load_file ".gtfsrc"


def fetch(link, name)
    system('curl', '-#', '-o', name, link.to_s)
end

def fetch_lastmod(link)
    resp    = IO.popen(['curl', '-s', '-I', link.to_s]) {|io| io.read }
    lastmod = resp.split(/\r?\n/)
                  .map    {|line| line.split(':', 2)   }
                  .select {|k, v| k == 'Last-Modified' }
                  .map    {|k, v| v                    }
                  .first
    
    Time.parse(lastmod) rescue nil
end

def short_link(link)
    link  = URI(link)
    path  = link.path
    host  = link.host

    if File.dirname(path) != '/'
        path = '/../' + File.basename(link.path)
    end

    if (shost = link.host.split('.')).size > 3
        host = (shost[0..0]+shost[1..-3].map{''}+shost[-2..-1]).join('.')
    end

    host + path
end

directory DOWNLOAD_DIR

# Fetch
desc "Fetch all GTFS data provided in .gtfsrc"
task :fetch => DOWNLOAD_DIR

# Build all remote GTFS targets
$cfg[:gtfs].each {|link|
    task :fetch => link
    task link do |t|
        # Link / Filename
        link     = URI(t.name)
        filename = Digest::SHA256.hexdigest(link.host)[0,8] +
                   '-' + File.basename(link.path)
        filepath = File.join(DOWNLOAD_DIR, filename)

        # Find information about last version
        time = fetch_lastmod(link)
        
        # Fetch last version if not uptodate
        if !File.exists?(filepath) || (time > File.mtime(filepath))
            puts "Fetching: #{filename}"
            fetch(link, filepath)
            File.utime(time, time, filepath)
        end        
    end
}

desc "List registered GTFS provider"
task :list do
    list     = $cfg[:gtfs].each.with_index.map {|p,i|
        [i+1, short_link(p)]
    }
    numsize  = [list.last[0].to_s.size, 2].max
    maxwidth = 78
    rowfmt   = " %#{numsize}d %-#{maxwidth-numsize-1}s"
    hdrfmt   = " %.#{numsize}s %-#{maxwidth-numsize-1}s"

    puts hdrfmt % ['id', 'url' ]
    list.each {|a| puts (rowfmt % a) }
end

desc "Initialize the database"
task 'db:init' do
    dbcfg  = $cfg[:db_gtfs_rw]
    dbpath = dbcfg[:adapter] + '://' + dbcfg[:host] + '/' +dbcfg[:database]
    puts "Run: " + ['bundle', 'exec', 'sequel', '-m', 'config/db', dbpath].join(' ')
end

desc "Cleanup the database"
task 'db:clean' do
    puts "TODO"
end

desc "Import GTFS into the database"
task 'db:import', [:id] do
task 'db:import', [:id] do |t,args|
    # Link / Filename
    link = URI($cfg[:gtfs][(args[:id].to_i - 1)])
    filename = Digest::SHA256.hexdigest(link.host)[0,8] +
               '-' + File.basename(link.path)
    filepath = File.join(DOWNLOAD_DIR, filename)

    # Import
    sh  'bundle', 'exec', 'ruby', './import.rb', args[:id], filepath
end

