# gtfs-db

Import GTFS files in a database



This work was supported by the [UrPolSens](http://imu.universite-lyon.fr/projet/urpolsens-reseaux-de-capteurs-sans-fil-pour-le-suivi-de-la-pollution-urbaine-wireless-sensor-networks-for-urban-pollution-monitoring/) project funded by LABEX IMU (ANR-10-LABX-0088) of Université de Lyon, within the program "Investissements d’Avenir" (ANR-11- IDEX-0007) operated by the French National Research Agency (ANR) 
