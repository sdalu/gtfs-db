require 'sequel'
require 'yaml'

require_relative 'lib/gtfs'
require_relative 'lib/gtfs/db-import'

#
# Read configuration
#
$cfg = YAML.load_file ".gtfsrc"
$cfg[:db_gtfs_rw].merge!(:pool_timeout        => 10,
                         :encoding            => 'utf8')
#
# Initialise DB handler
#
DB = Sequel.connect $cfg[:db_gtfs_rw]


#
# Import
#
$db = GTFS::DBImport.new(DB)
$db.import(ARGV[0].to_i, ARGV[1])




